from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 33")


def convert_to_rdd(x):
    return sc.parallelize(x)


employee_name = sc.textFile("spark5/EmployeeName.csv")
employee_name_pairRDD = employee_name.map(lambda x: (x.split(",")[0], x.split(",")[1]))

employee_salary = sc.textFile("spark5/EmployeeSalary.csv")
employee_salary_pairRDD = employee_salary.map(lambda x: (x.split(",")[0], x.split(",")[1]))

joined = employee_name_pairRDD.join(employee_salary_pairRDD)
key_removed = joined.values()
key_removed_swap = key_removed.map(lambda x: (x[1], x[0]))

## Case 1
grp_by_key = key_removed_swap.groupByKey().map(lambda x: (x[0], list(x[1]))).collect()
grp_by_key_list = list(map(convert_to_rdd, grp_by_key))

for index, item in enumerate(grp_by_key_list):
    item.saveAsTextFile("spark5/Employee" + str(index))

## Case 2
# grp_by_key = key_removed_swap.groupByKey().map(lambda x: (x[0], list(x[1]))).collect()
# grp_by_key_rdd = sc.parallelize(grp_by_key)
# grp_by_key_rdd.saveAsTextFile("spark5/test")