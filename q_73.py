from pyspark import SQLContext, SparkContext, SparkConf
from pyspark.sql import SparkSession
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
# conf = SparkConf().setAppName("Question 73")
# sc = SparkContext(conf=conf)
# sql_context = SQLContext(sparkContext=sc)
spark = SparkSession.builder.master("local").appName("Question 73").getOrCreate()


@logger.catch
def main():
    employee = spark.read.json("employee.json")
    employee.createOrReplaceTempView("EmployeeTable")
    employee_info = spark.sql("SELECT * FROM EmployeeTable")
    for row in employee_info.collect():
        print(row)
    # employee_info.toJSON().saveAsTextFile("employeeJson1")
    employee_info.toJSON().saveAsTextFile("hdfs://sandbox-hdp.hortonworks.com:8020/user/practice_spark/employeeJson1")


if __name__ == '__main__':
    main()
