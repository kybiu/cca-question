from pyspark import SparkContext
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 68")


@logger.catch
def main():
    file = sc.textFile("spark75/file1.txt")
    sentences = file.glom().map(lambda x: " ".join(x)).flatMap(lambda x: x.split("."))
    bigrams = sentences.map(lambda x: x.split()).flatMap(lambda x: [((x[i], x[i + 1]), 1) for i in range(0, len(x) - 1)])
    freq_bigrams = bigrams.reduceByKey(lambda x, y: x + y).map(lambda x: (x[1], x[0])).sortByKey(False)
    freq_bigrams.take(10)
    a = 0


if __name__ == '__main__':
    main()
