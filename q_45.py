from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 45")


def main():
    technology = sc.textFile("spark12/technology.txt").map(lambda x: x.split(","))
    salary = sc.textFile("spark12/salary.txt").map(lambda x: x.split(","))
    joined = technology.map(lambda x: ((x[0], x[1]), x[2])).join(salary.map(lambda x: ((x[0], x[1]), x[2])))
    joined_reformat = joined.map(lambda x: (*x[0], *x[1]))
    joined_reformat.map(lambda x: ", ".join(map(str, x))).repartition(1).saveAsTextFile("spark12/result")


if __name__ == '__main__':
    main()
