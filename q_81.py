from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, IntegerType, StringType, FloatType
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

spark = SparkSession.builder.master("local").appName("Question 81").getOrCreate()

product_schema = StructType() \
    .add("productid", IntegerType(), True) \
    .add("code", StringType(), True) \
    .add("name", StringType(), True) \
    .add("quantity", IntegerType(), True) \
    .add("price", FloatType(), True)


@logger.catch
def main():
    # product = spark.sparkContext.textFile("hdfs://sandbox-hdp.hortonworks.com:8020/user/practice_spark/product.csv")
    product = spark.read \
        .options(header='True', delimiter=',') \
        .schema(product_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/practice_spark/product.csv")

    product.createOrReplaceTempView("product")

    ## QUESTION 81
    # product.write.mode(saveMode="overwrite").format("orc").saveAsTable("product_orc_table")
    # product.write.mode(saveMode="overwrite").format("parquet").saveAsTable("product_parquet_table")

    ## QUESTION 82
    # a_82 = spark.sql("SELECT name FROM product where quantity <= 2000").show()
    # b_82 = spark.sql("SELECT name,price from PRODUCT  where code='PEN'").show()
    # c_82 = spark.sql("SELECT * from product where name like 'Pencil%'").show()
    # d_82 = spark.sql("SELECT * from product where name like 'P%'").show()

    ## QUESTION 83
    # a_83 = spark.sql("select * from product where quantity >= 5000 and name like 'Pen%'").show()
    # b_83 = spark.sql("select * from product where quantity >= 5000 and name like 'Pen%' and price < 1.24").show()
    # c_83 = spark.sql("select * from product where not (quantity >= 5000 and name like 'Pen%')").show()
    # d_83 = spark.sql("select * from product where name='Pen Red' or name ='Pen Black'").show()
    # e_83 = spark.sql("select * from product where price between 1.0 and 2.0 and  quantity between 1000 and 2000").show()

    ## QUESTION 84
    # a_84 = spark.sql("select * from product  where code is null").show()
    # b_84 = spark.sql("select * from product where name like 'Pen%' order by price desc").show()
    # c_84 = spark.sql("select * from product where name like 'Pen%' order by price desc, quantity asc").show()
    # d_84 = spark.sql("select * from product where order by price desc limit 2").show()

    ## QUESTION 85
    # a_85 = spark.sql("select productid as ID, code as Code, name as Description, price as UnitPrice from  product").show()
    # b_85 = spark.sql("select concat(code,'-',name) from product").show()
    # c_85 = spark.sql("select distinct price from product").show()
    # d_85 = spark.sql("select distinct price, name from product").show()
    # e_85 = spark.sql("select * from product order by productid, code").show()
    # f_85 = spark.sql("select count(*) from product").show()
    # g_85 = spark.sql("select count(*), code from product group by code").show()

    ## QUESTION 86
    a_86 = spark.sql("select max(price) as Max, min(price) as Min, avg(price) as AVG, std(price) as STD, sum(price) as Sum from product").show()
    b_86 = spark.sql("select code, max(price) as max, min(price) as min from product group by code").show()
    c_86 = spark.sql("select max(price) as Max, min(price) as Min, cast(avg(price) as decimal(7,2))as AVG, cast(std(price) as decimal(7,2)) as STD, sum(price) as Sum from product").show()
    d_86 = spark.sql("select code, count(*) as Count, avg(price) from product group by code having Count >= 3").show()
    e_86 = spark.sql("select code, max(price) as Max, min(price) as Min, cast(avg(price) as decimal(7,2))as AVG, sum(price) as Sum from product group by code with rollup ").show()


if __name__ == '__main__':
    main()
