from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 35")


def main():
    employee_name = sc.textFile("spark7/EmployeeName.csv")
    employee_name_rdd = employee_name.map(lambda x: (x.split(",")[0], x.split(",")[1]))
    employee_name_rdd_sorted = employee_name_rdd.sortBy(lambda x: x[1])
    employee_name_rdd_sorted.repartition(1).saveAsTextFile("spark7/result")


if __name__ == '__main__':
    main()
