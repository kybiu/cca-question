from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 34")


def remove_space(x):
    return x.strip()


def main():
    user = sc.textFile("spark6/user.csv")
    header_and_row = user.map(lambda x: list(map(remove_space, x.split(","))))
    header = header_and_row.first()
    data = header_and_row.filter(lambda x: x[0] != header[0])
    map_data = data.map(lambda x: dict(zip(header, x)))
    result = map_data.filter(lambda x: x["id"] != "myself")
    result.saveAsTextFile("spark6/result")


if __name__ == '__main__':
    main()
