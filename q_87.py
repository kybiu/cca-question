from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, IntegerType, StringType, FloatType
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

spark = SparkSession.builder.master("local").appName("Question 87").getOrCreate()

product_schema = StructType() \
    .add("product_id", IntegerType(), False) \
    .add("product_code", StringType(), False) \
    .add("name", StringType(), True) \
    .add('quantity', IntegerType(), True) \
    .add('price', FloatType(), True) \
    .add('supplier_id', IntegerType(), True)

supplier_schema = StructType() \
    .add('supplier_id', IntegerType(), True) \
    .add('name', StringType(), True) \
    .add('phone', IntegerType(), True)

products_suppliers_schema = StructType() \
    .add('product_id', IntegerType(), False) \
    .add('supplier_id', IntegerType(), False)


@logger.catch
def main():
    product = spark.read \
        .options(header='True', delimiter=',') \
        .schema(product_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql2/product.csv")
    product.createOrReplaceTempView("product")

    supplier = spark.read \
        .options(header='True', delimiter=',') \
        .schema(supplier_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql2/supplier.csv")
    supplier.createOrReplaceTempView("supplier")

    products_suppliers = spark.read \
        .options(header='True', delimiter=',') \
        .schema(products_suppliers_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql2/products_suppliers.csv")
    products_suppliers.createOrReplaceTempView("products_suppliers")

    ## Question 87
    # result = spark.sql("select p.name as prod_name, p.price, s.name as sup_name from product p join supplier s on s.supplier_id = p.supplier_id where price < 0.6")
    # result.show()

    ## Question 88
    # a_88 = spark.sql("select p.name as prod_name, p.price, s.name as sup_name \
    #  from products_suppliers ps join product p on ps.product_id = p.product_id join supplier s on ps.supplier_id = s.supplier_id").show()

    # b_88 = spark.sql("select s.name as supp_name from products_suppliers as ps join supplier s on ps.supplier_id = s.supplier_id join product p on ps.product_id = p.product_id \
    # where p.name = 'Pencil 3B'").show()

    # c_88 = spark.sql \
    #     ("""
    # select distinct p.product_id as prod_id from \
    # product p, products_suppliers ps join supplier s on s.supplier_id = ps.supplier_id\
    # where p.supplier_id = s.supplier_id  and s.name = 'ABC Traders'
    # """)
    # c_88.show()


if __name__ == '__main__':
    main()
