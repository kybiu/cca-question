from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, IntegerType, StringType, FloatType, DateType, TimestampType
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

spark = SparkSession.builder.master("local").appName("Question 89").getOrCreate()

patients_schema = StructType() \
    .add("patiend_id", IntegerType(), False) \
    .add("name", StringType(), False) \
    .add("dob", StringType(), False) \
    .add("last_visit_date", StringType(), False)


@logger.catch
def main():
    patients = spark.read \
        .options(header=True, delimiter=',') \
        .schema(patients_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql3/patients.csv")

    patients.createOrReplaceTempView("patients")

    # test = spark.sql("select * from patients").show()

    # a_89 = spark.sql("""
    # select * from patients
    # where to_date(CAST(UNIX_TIMESTAMP(last_visit_date, 'yyyy-MM-dd') AS TIMESTAMP)) between '2019-09-15' and current_date()
    # order by last_visit_date
    # """).show()
    #
    # b_89 = spark.sql("""
    # select * from patients
    # where YEAR(TO_DATE(CAST(UNIX_TIMESTAMP(last_visit_date, 'yyyy-MM-dd') AS TIMESTAMP))) = 2011
    # """).show()

    # c_89 = spark.sql("""
    # select name, dob, datediff(current_date(), TO_DATE(CAST(UNIX_TIMESTAMP(dob, 'yyyy-MM-dd') as TIMESTAMP)))/365 as age
    # from patients
    # """).show()

    e_89 = spark.sql("""
   select name, dob
   from patients       
   where TO_DATE(CAST(UNIX_TIMESTAMP(dob, 'yyyy-MM-dd') as TIMESTAMP)) >= DATE_SUB(current_date(), 18 *365)
   """).show()


if __name__ == '__main__':
    main()
