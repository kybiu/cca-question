from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, IntegerType, StringType
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

spark = SparkSession.builder.master("local").appName("Question 90").getOrCreate()

course_schema = StructType() \
    .add("id", IntegerType(), False) \
    .add("course", StringType(), False)

fee_schema = StructType() \
    .add("id", IntegerType(), False) \
    .add("fee", IntegerType(), False)


def main():
    course = spark.read \
        .options(header=True, delimiter=",") \
        .schema(course_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql4/course.txt")

    course.createOrReplaceTempView("course")
    fee = spark.read \
        .options(header=True, delimiter=",") \
        .schema(course_schema) \
        .csv("hdfs://sandbox-hdp.hortonworks.com:8020/user/sparksql4/fee.txt")
    fee.createOrReplaceTempView("fee")

    # test = spark.sql("select * from course").show()

    a_90 = spark.sql("select * from course c left join fee f on c.id = f.id").show()
    b_90 = spark.sql("select * from fee f left join course c on c.id = f.id").show()


if __name__ == '__main__':
    main()
