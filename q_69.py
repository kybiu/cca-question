from pyspark import SparkContext, SparkConf
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

conf = SparkConf().setAppName("CCA 175 Problem 84")
sc = SparkContext(conf=conf)


@logger.catch
def main():
    content_rdd = sc.textFile("Content.txt")
    no_empty_lines = content_rdd.filter(lambda x: len(x) > 0)
    words = no_empty_lines.flatMap(lambda x: x.split(" "))
    final_rdd = words.filter(lambda x: len(x) > 2)
    for word in final_rdd.collect():
        print(word)


if __name__ == '__main__':
    main()
