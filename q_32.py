from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 32")
content = sc.textFile("spark3/sparkdir1/file1.txt,spark3/sparkdir2/file2.txt,spark3/sparkdir3/file3.txt")
flat_content = content.flatMap(lambda x: x.split(" "))
trimmed_content = flat_content.map(lambda x: x.strip())

removeRDD = sc.parallelize(["a", "the", "an", "as", "a", "with", "this", "these", "is", "are", "in", "for", "to", "and", "The", "of"])

filtered_trimmed_content = trimmed_content.subtract(removeRDD)

pairRDD = filtered_trimmed_content.map(lambda x: (x, 1))
word_count = pairRDD.reduceByKey(lambda x, y: x + y)

# sorted_output = word_count.sortByKey(False)
sorted_output = word_count.sortBy(lambda x: x[1], ascending=False)

sorted_output.saveAsTextFile("spark3/result")
sorted_output.saveAsTextFile(path="spark3/compressed_result",compressionCodecClass="org.apache.hadoop.io.compress.GzipCodec")
