from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 31")
content = sc.textFile("spark2/Content.txt")
remove = sc.textFile("spark2/Remove.txt")

removeRDD = remove.flatMap(lambda x: x.split(",")).map(lambda x: x.strip())
b_remove = sc.broadcast(removeRDD.collect())

words = content.flatMap(lambda line: line.split(" "))

filtered = words.filter(lambda x: x not in b_remove.value)

pairRDD = filtered.map(lambda x: (x, 1))
word_count = pairRDD.reduceByKey(lambda x, y: x + y)
word_count.saveAsTextFile("spark2/result")
