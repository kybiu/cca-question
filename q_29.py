from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 29")
manager = sc.textFile("spark1/EmployeeManager.csv")
manager_pair_RDD = manager.map(lambda x: (x.split(",")[0], x.split(",")[1]))

name = sc.textFile("spark1/EmployeeName.csv")
name_pair_RDD = name.map(lambda x: (x.split(",")[0], x.split(",")[1]))

salary = sc.textFile("spark1/EmployeeSalary.csv")
salary_pair_RDD = salary.map(lambda x: (x.split(",")[0], x.split(",")[1]))

joined = name_pair_RDD.join(manager_pair_RDD).join(salary_pair_RDD)
joined_data = joined.sortByKey()

final_data = joined_data.map(lambda x: (x[0], x[1][0][0], x[1][0][1], x[1][1]))

a = 0
final_data.saveAsTextFile(path="spark1/result.txt")
