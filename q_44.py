from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 44")


def main():
    result_x = []

    for i in range(1, 5):
        file_path = "spark11/file{id}.txt".format(id=i)
        file = sc.textFile(file_path)
        file_rdd = file.flatMap(lambda x: x.split(" "))
        file_rdd_pair = file_rdd.map(lambda x: (x, 1))
        file_rdd_sum_occur = file_rdd_pair.reduceByKey(lambda x, y: x + y)
        file_rdd_sum_occur_sort = file_rdd_sum_occur.sortBy(lambda x: x[1], ascending=False)
        file_result = sc.parallelize(("file{id}".format(id=i), file_rdd_sum_occur_sort.first()[0], file_rdd_sum_occur_sort.first()[1]))
        file_result_list = file_result.collect()
        result_x.append(file_result_list)

    result = sc.parallelize(result_x)
    result.map(lambda x: ", ".join(map(str, x))).saveAsTextFile("spark11/result")


if __name__ == '__main__':
    main()
