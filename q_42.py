from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 42")


def main():
    sales_rdd = sc.textFile("spark10/sales.txt").map(lambda x: x.split(","))

    headers = sales_rdd.first()
    sales_rdd_noheader = sales_rdd.filter(lambda x: x[0] != headers[0])

    sales_rdd_reformat = sales_rdd_noheader.map(lambda x: ((x[0], x[1], x[3]), (1, float(x[2]))))
    result = sales_rdd_reformat.reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1]))
    result_reformat = result.map(lambda x: (*x[0], *x[1]))
    result_reformat.map(lambda x: ", ".join(map(str, x))).repartition(1).saveAsTextFile("spark10/group.txt")


if __name__ == '__main__':
    main()
