from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 36")


def create_combiner(x):
    return [x]


def merge_value(x, y):
    x.append(y)
    return x


def merge_combiner(x, y):
    x.extend(y)
    return x


def main():
    data = sc.textFile("spark8/data.csv")
    data_pair_rdd = data.map(lambda x: x.split(","))

    combined_output = data_pair_rdd.combineByKey(create_combiner, merge_value, merge_combiner)
    combined_output.repartition(1).saveAsTextFile("spark8/result")


if __name__ == '__main__':
    main()
