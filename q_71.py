from pyspark import SparkContext, SparkConf

import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
conf = SparkConf().setAppName("Question 71")
sc = SparkContext(conf=conf)


@logger.catch
def main():
    content_rdd = sc.textFile("Content.txt")
    no_empty_lines = content_rdd.filter(lambda x: len(x) > 0)
    content_pair = no_empty_lines.map(lambda x: (x.split(" ")[0], x))
    content_pair.repartition(1).saveAsSequenceFile("problem86")

    content_pair_2 = no_empty_lines.map(lambda x: (None, x))
    content_pair_2.repartition(1).saveAsSequenceFile("problem86_1")

    seq_rdd = sc.sequenceFile("problem86_1")
    for line in seq_rdd.collect():
        print(line)
    a = 0


if __name__ == '__main__':
    main()
