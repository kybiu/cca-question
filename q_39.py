from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 39")


def main():
    one = sc.textFile("spark16/file1.txt")
    two = sc.textFile("spark16/file2.txt")

    one_rdd = one.map(lambda x: (x.split(",")[0], (x.split(",")[1], x.split(",")[2])))
    two_rdd = two.map(lambda x: (x.split(",")[0], (x.split(",")[1], x.split(",")[2])))

    joined = one_rdd.join(two_rdd)
    second_column = joined.map(lambda x: int(x[1][0][1]))
    total = second_column.reduce(lambda a, b: a + b)
    a = 0


if __name__ == '__main__':
    main()
