from pyspark import SparkContext
import os

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

sc = SparkContext("local", "Question 46")


def main():
    rdd = sc.parallelize([
        ("Deeapak", "male", 4000),
        ("Deepak", "male", 2000),
        ("Deepika", "female", 2000),
        ("Deepak", "female", 2000),
        ("Deepak", "male", 1000),
        ("Neeta", "female", 2000)
    ])

    rdd_by_key = rdd.map(lambda x: ((x[0], x[1]), x[2]))
    rdd_group = rdd_by_key.groupByKey()
    result = rdd_group.map(lambda x: (x[0][0], x[0][1], sum(list(x[1]))))
    result.map(lambda x: ", ".join(map(str, x))).repartition(1).saveAsTextFile("spark12/result2")


if __name__ == '__main__':
    main()
