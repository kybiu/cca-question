from pyspark import SparkContext, SparkConf
import os
from loguru import logger

os.environ['PYSPARK_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/root/miniconda3/envs/py38/bin/python3'

spark_conf = SparkConf().setAppName("Question 70")
sc = SparkContext(conf=spark_conf)


def main():
    content_rdd = sc.textFile("Content.txt")
    words = content_rdd.flatMap(lambda x: x.split(" "))
    words_pair = words.map(lambda x: (x, 1))
    words_count = words_pair.reduceByKey(lambda a, b: a + b)
    words_count.map(lambda x: ", ".join(map(str, x))).repartition(1).saveAsTextFile("problem85")


if __name__ == '__main__':
    main()
